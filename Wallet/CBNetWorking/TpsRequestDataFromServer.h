//
//  TpsRequestDataFromServer.h
//  TPS
//
//  Created by Tps on 16/5/13.
//  Copyright © 2016年 Tps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CaptchaSuccessBlock)();
typedef void(^requestSuccessBlock)(NSDictionary *json,NSError *error);
typedef void(^requestErrorBlock)(NSError *error);


typedef NS_ENUM(NSInteger, RequestServerData)
{
    RequestServerDataWithNormal = 0,     //!< 数据正常
    
    RequestServerDataWithNull,           //!< 数据为空
    
    RequestServerDataWithFormatError,   //!< 有数据 解析失败
    
    RequestServerDataWithNetworkError, //!< 网络出错
    
    RequestServerDataWithOther        //!< 未定义（其他原因）
};

@interface TpsRequestDataFromServer : NSObject

@property (nonatomic, copy) requestSuccessBlock requestSuccessBlock;
//@property (nonatomic, copy) requestModelBlock requestModelBlock;


@property (nonatomic, strong) NSURLSessionDataTask *dataTask;

@property (nonatomic, assign) NSUInteger timeoutInterval;//!< 请求超时  默认是30s



+ (instancetype)shareManager;
/**
 * 请求数据
 */
- (NSURLSessionDataTask *)requestFromServerWithUrl:(NSString *)url parameters:(NSDictionary *)parameters result:(requestSuccessBlock)result;

- (BOOL)examineNet;

/**
 * 请求查询PayPal是否绑定
 */
//-(void)UnbundlingAccountResult:(requestModelBlock)result;
/**
 * 获取验证码 
 * dic : 请求参数
 * successBlock :成功返回
 */
-(void)send_captcha:(NSDictionary *)dic fromVC:(UIViewController *)fromVC whileSuccess:(CaptchaSuccessBlock)successBlock;



/**
 获取当前网络状态

 @return @“WIFI,3G”
 */
- (NSString *)getCurrentNetStatus;

/**
 获取公共参数

 @return 公共参数（包含：设备信息与scid，没有语言id）
 */
+ (NSMutableDictionary *)getPublicParameter;

///**
// 加密参数
//
// @param parame 需要加密的参数
// @return 加密后的字符串
// */
//+ (NSString *)configParame:(NSDictionary *)parame;
//
///**
// 字典转字符串
//
// @param parame 字典
// @return 字符串
// */
//+ (NSString *)parameString:(NSDictionary *)parame;
@end
