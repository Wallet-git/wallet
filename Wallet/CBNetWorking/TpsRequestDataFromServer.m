//
//  TpsRequestDataFromServer.m
//  TPS
//
//  Created by Tps on 16/5/13.
//  Copyright © 2016年 Tps. All rights reserved.
//

#import "TpsRequestDataFromServer.h"
#import "AFHTTPSessionManager.h"
#import "AFURLResponseSerialization.h"

@interface TpsRequestDataFromServer ()
@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, strong) AFHTTPResponseSerializer *response;

@end

@implementation TpsRequestDataFromServer

+ (instancetype)shareManager
{
    static TpsRequestDataFromServer *requestManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requestManager = [[TpsRequestDataFromServer alloc] init];
        
    });
    return requestManager;
}


- (instancetype)init
{
    if (self = [super init]) {
        _manager = [AFHTTPSessionManager manager];
        _response = [AFHTTPResponseSerializer serializer];
    }
    return self;
}

- (NSURLSessionDataTask *)requestFromServerWithUrl:(NSString *)url parameters:(NSDictionary *)parameters result:(requestSuccessBlock)result
{
    
    NSMutableDictionary *mubParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    // 获取公共参数
    NSMutableDictionary *publicParameter = [TpsRequestDataFromServer getPublicParameter];
    [mubParameters addEntriesFromDictionary:publicParameter];
    __weak TpsRequestDataFromServer *weakSelf = self;
    if (self.timeoutInterval == 0) {
        _manager.requestSerializer.timeoutInterval = 30;
    }else{
        _manager.requestSerializer.timeoutInterval = self.timeoutInterval;
    }
    _manager.responseSerializer = _response;
    
//    // 加上这行代码，https ssl 验证。
//    [_manager setSecurityPolicy:[AFSecurityPolicy customSecurityPolicy]];
    
    self.dataTask = [_manager POST:url parameters:mubParameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSInteger statusCode = response.statusCode;
        
        if (statusCode == 200) {
            // 删除批次号
            [mubParameters removeObjectForKey:@"ft"];
            NSURL *URL = response.URL;
            NSString *urlString = [URL absoluteString];
//            [TpsRequestTokenModel deleteRequest_BatchTableDataWithRequestUrl:urlString parameter:[TpsRequestDataFromServer parameString:mubParameters]];
            NSLog(@"urlString == %@",urlString);
        }else{
            // 不删除批次号，让token过期
            NSLog(@"请求失败------------------------");
        }
        
        NSError *error = nil;
        
        weakSelf.timeoutInterval = 0;
        
        NSString * responseString = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
   
        NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
        NSMutableDictionary *json = [[NSMutableDictionary alloc] initWithDictionary:jsonDic];
        
        // 检测是否需要退出App
        __weak TpsRequestDataFromServer *weakSelf = self;
        if (![json[@"clear"] isEqualToString:@"0"] && json[@"clear"] != nil) {

        }
        
        //***⬇️*账户违规操作******//
        if ([json[@"error_code"] isEqual:@(888888)]){

        }else if ([json[@"error_code"] isEqual:@(666666)]){
            //账户被挤下线
            //[TpsLoginManager LogoutProcessing:json[@"data"][@"msg"]];
        }else if ([json[@"error_code"] isEqual:@(1070)]){
            //账户被挤下线
          //  [TpsLoginManager LogoutProcessing:json[@"error_msg"]];
        }else{
            if (error == nil) {
                result(json,error);
            }else{
                result(nil,error);
            }
        }
        //***⬆️账户违规操作******//
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        result(nil,error);
        
        weakSelf.timeoutInterval = 0;
        NSLog(@"请求失败------------------------");
    }];
    
    return self.dataTask;
   
}

- (BOOL)examineNet
{
    //1.创建网络状态监测管理者
    AFNetworkReachabilityManager *manger = [AFNetworkReachabilityManager sharedManager];
    
    __block BOOL ret;
    
    //2.监听改变
    [manger setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        /*
         AFNetworkReachabilityStatusUnknown          = -1,
         AFNetworkReachabilityStatusNotReachable     = 0,
         AFNetworkReachabilityStatusReachableViaWWAN = 1,
         AFNetworkReachabilityStatusReachableViaWiFi = 2,
         */
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
//                NSLog(@"未知");
                 ret = YES;
                break;
            case AFNetworkReachabilityStatusNotReachable:
//                NSLog(@"没有网络");
                 ret = NO;
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
//                NSLog(@"3G|4G");
                 ret = YES;
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
//                NSLog(@"WiFi");
                 ret = YES;
                break;
            default:
                break;
        }
    }];
    
    [manger startMonitoring];
//    NSLog(@"%@",ret ? @"有网络" : @"无网络");
    return ret;
}

- (NSString *)getCurrentNetStatus
{
    //1.创建网络状态监测管理者
    AFNetworkReachabilityManager *manger = [AFNetworkReachabilityManager sharedManager];
    
    __block NSString *statusString;
    
    //2.监听改变
    [manger setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        /*
         AFNetworkReachabilityStatusUnknown          = -1,
         AFNetworkReachabilityStatusNotReachable     = 0,
         AFNetworkReachabilityStatusReachableViaWWAN = 1,
         AFNetworkReachabilityStatusReachableViaWiFi = 2,
         */
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                statusString = @"未知";
                break;
            case AFNetworkReachabilityStatusNotReachable:
                statusString = @"没有网络";
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                statusString = @"3G|4G";
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                statusString = @"WiFi";
                break;
            default:
                break;
        }
    }];
    
    [manger startMonitoring];
    return statusString;
}

+ (NSMutableDictionary *)getPublicParameter
{
    NSMutableDictionary *publicParameter = [[NSMutableDictionary alloc] init];
    // 添加公共参数
//    [publicParameter setValue:[TpsDevice getPortSource] forKey:@"platform"];//!< 手机品牌  Android or iOS
//    [publicParameter setValue:[TpsDevice getPhoneBrand] forKey:@"brand"];//!< iPhone
//    [publicParameter setValue:[TpsDevice getPhoneModel] forKey:@"model"];//!< 手机型号
//    [publicParameter setValue:[TpsDevice getPhoneSystemVersion] forKey:@"os_version"];//!< 系统版本
//    [publicParameter setValue:[TpsDevice getPhoneUUID] forKey:@"deviceId"];//!< UUID
//    [publicParameter setValue:[TpsDevice getAppVersion] forKey:@"app_version"];//!< app 版本
//    [publicParameter setValue:Request_app_url forKey:@"app_url"];//!< 哪个
//
//    NSString *scid = [[NSUserDefaults standardUserDefaults] valueForKey:SCID] ? [[NSUserDefaults standardUserDefaults] valueForKey:SCID] : @"";
//    [publicParameter setValue:scid forKey:@"scid"];//!< app 版本
    return publicParameter;
}


- (NSString *)jsonKeyAddValueWith:(id)json
{
    id data = json[@"data"];
    id error_code = json[@"error_code"];
    NSMutableString *keyValueString = [[NSMutableString alloc] init];
    [keyValueString appendFormat:@"%@",error_code];
    if ([data isKindOfClass:[NSDictionary class]]) {
        // data 是字典类型
        // key 先排序
        NSDictionary *dataDic = (NSDictionary *)data;
        NSArray *keys = dataDic.allKeys;
        keys = [keys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            
            NSString *first = obj1;
            NSString *second = obj2;
            
            return [first compare:second];
        }];
        // 再取值
        for (NSString *key in keys) {
            if (![dataDic[key] isKindOfClass:[NSArray class]] &&
                ![dataDic[key] isKindOfClass:[NSDictionary class]] &&
                ![dataDic[key] isKindOfClass:NSClassFromString(@"__NSCFBoolean")] && dataDic[key] != [NSNull null]) {
                [keyValueString appendFormat:@"%@%@",key,dataDic[key]];
            }
        }
    }
    return keyValueString;
}

@end
