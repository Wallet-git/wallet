//
//  CBContactViewController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/10.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBContactViewController.h"

@interface CBContactViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *topImage;

@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *sellBtn;

@property (weak, nonatomic) IBOutlet UILabel *choseBuyType;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
@property (weak, nonatomic) IBOutlet UIView *buyBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *minToMax;
@property (weak, nonatomic) IBOutlet UITextField *minTextfield;
@property (weak, nonatomic) IBOutlet UITextField *maxTextfield;
@property (weak, nonatomic) IBOutlet UIView *midLine;
@property (weak, nonatomic) IBOutlet UILabel *todayLable;
@property (weak, nonatomic) IBOutlet UILabel *numberLable;
@property (weak, nonatomic) IBOutlet UILabel *ciLable;

@property (weak, nonatomic) IBOutlet UIImageView *wechatImage;
@property (weak, nonatomic) IBOutlet UIImageView *ailpayImage;
@property (weak, nonatomic) IBOutlet UIImageView *bankImage;

@property (weak, nonatomic) IBOutlet UIView *buyNumberView;
@property (weak, nonatomic) IBOutlet UIView *choseView;
@property (weak, nonatomic) IBOutlet UIButton *upBtn;
@property (strong, nonatomic) IBOutlet UIView *bottonScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottonHeight;//滚动距离上方视图高度
@property (weak, nonatomic) IBOutlet UIImageView *BLViewImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottonViewHeight;//下方波浪视图高度
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *OrderHeight;//接单按钮高度
@property (nonatomic, assign) CGFloat recording;
@end

@implementation CBContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self greatUI];
    
}

- (void)greatUI{
    
    [self.topImage setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"iPhone XR背景"]]];
    [self.BLViewImage setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"路径 5"]]];
    self.buyNumberView.alpha = 0;
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F9F9FC"];
    self.choseView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    self.buyNumberView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    
    [self.buyBtn setTitle:@"买入" forState:UIControlStateNormal];
    [self.sellBtn setTitle:@"卖出" forState:UIControlStateNormal];
    [self.buyBtn setTitleColor:[UIColor colorWithHexString:@"#3B67E0"] forState:UIControlStateNormal];
    [self.sellBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.buyBtn.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    
    self.wechatBtn.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    self.alipayBtn.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    self.bankBtn.backgroundColor = [UIColor colorWithHexString:@"#EBEBEB"];
    [self.wechatBtn setTitleColor:[UIColor colorWithHexString:@"#C7C5C5"] forState:UIControlStateNormal];
    [self.alipayBtn setTitleColor:[UIColor colorWithHexString:@"#C7C5C5"] forState:UIControlStateNormal];
    [self.bankBtn setTitleColor:[UIColor colorWithHexString:@"#C7C5C5"] forState:UIControlStateNormal];
    
    
    
    self.choseBuyType.text = @"请选择所需匹配的支付方式";
    self.choseBuyType.textColor = [UIColor colorWithHexString:@"#A9A9A9"];
    self.minToMax.text = @"最小最大买入值(选填)";
    self.minToMax.textColor = [UIColor colorWithHexString:@"#A9A9A9"];
    self.buyBottonLine.backgroundColor = [UIColor colorWithHexString:@"#F3F3F3"];
    self.midLine.backgroundColor = [UIColor colorWithHexString:@"#D8D8D8"];
    self.todayLable.text = @"今日剩余买入次数式";
    self.todayLable.textColor = [UIColor colorWithHexString:@"#A9A9A9"];
    self.ciLable.textColor = [UIColor colorWithHexString:@"#A9A9A9"];
    
    
    if (IS_IPHONE_Xr || IS_IPHONE_Xs_Max) {
        self.scrollViewBottonHeight.constant = - 65;
        self.bottonViewHeight.constant = 480;
        self.OrderHeight.constant = 95;
    }else if (IS_IPHONE_X){
        self.bottonViewHeight.constant = 380;
        self.scrollViewBottonHeight.constant = - 65;
        self.OrderHeight.constant = 95;
    }else if (kiPhone6Plus){
        self.bottonViewHeight.constant = 380;
        self.scrollViewBottonHeight.constant = - 90;
        self.OrderHeight.constant = 75;
    }else{
        self.scrollViewBottonHeight.constant = - 90;
        self.bottonViewHeight.constant = 310;
        self.OrderHeight.constant = 75;
    }
}




//展开视图
- (IBAction)upBTN:(UIButton *)sender {

        self.upBtn.alpha = 0;
        self.buyNumberView.alpha = 1;
        self.scrollViewBottonHeight.constant = 14;
        
        if (IS_IPHONE_Xr || IS_IPHONE_Xs_Max) {
            self.OrderHeight.constant = 180;
            self.bottonViewHeight.constant = 400;
        }else if (IS_IPHONE_X){
            self.OrderHeight.constant = 180;
            self.bottonViewHeight.constant = 300;
            
        }else if (kiPhone6Plus){
            self.OrderHeight.constant = 160;
            self.bottonViewHeight.constant = 270;
        }else{
            self.OrderHeight.constant = 160;
            self.bottonViewHeight.constant = 200;
        }
    
}

//收起视图
- (IBAction)topBTN:(UIButton *)sender {
    
    self.upBtn.alpha = 1;
    self.buyNumberView.alpha = 0;
    
    if (IS_IPHONE_Xr || IS_IPHONE_Xs_Max) {
        self.scrollViewBottonHeight.constant = - 65;
        self.bottonViewHeight.constant = 480;
        self.OrderHeight.constant = 95;
    }else if (IS_IPHONE_X){
        self.bottonViewHeight.constant = 380;
        self.scrollViewBottonHeight.constant = - 65;
        self.OrderHeight.constant = 95;
        
    }else if (kiPhone6Plus){
        self.bottonViewHeight.constant = 380;
        self.scrollViewBottonHeight.constant = - 90;
        self.OrderHeight.constant = 75;
    }else{
        self.scrollViewBottonHeight.constant = - 90;
        self.bottonViewHeight.constant = 310;
        self.OrderHeight.constant = 75;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}



- (IBAction)buyBTN:(UIButton *)sender {
    
}

- (IBAction)sellBTN:(UIButton *)sender {
    
    
}








- (IBAction)wechatBTN:(UIButton *)sender {
    
    
}

- (IBAction)alipayBTN:(UIButton *)sender {
    
    
}

- (IBAction)bankBTN:(UIButton *)sender {
    
    
}

@end
