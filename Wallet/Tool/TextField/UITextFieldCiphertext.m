//
//  UITextFieldCiphertext.m
//  UItextField明文密文切换
//
//  Created by stephen on 2017/12/5.
//  Copyright © 2017年 stephen. All rights reserved.
//

#import "UITextFieldCiphertext.h"

//  正文文字 （已确认）
#define TPSMainTextColor [UIColor colorWithRed:(62)/255.0f green:(58)/255.0f  blue:(57)/255.0f  alpha:1]
#define TPSPlaceholdColor [UIColor colorWithRed:(196)/255.0f green:(196)/255.0f  blue:(196)/255.0f  alpha:1]

@implementation UITextFieldCiphertext

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupUI];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}
#pragma mark 密码可视
-(void)secureTextEntry{
    
    if (self.secureTextEntry) {
        //明文
        //        self.img.image=[UIImage imageNamed:@"Group 1970"];
        for (UIView *v in self.rightView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                UIImageView *img = (UIImageView *)v;
                img.image=[UIImage imageNamed:@"眼睛睁开"];
            }
        }
        NSString *tempPwdStr = self.text;
        self.text = @""; // 这句代码可以防止切换的时候光标偏移
        self.text = tempPwdStr;
    }else{
        //        self.img.image=[UIImage imageNamed:@"Path 1578"];
        NSString *tempPwdStr = self.text;
        self.text = @""; // 这句代码可以防止切换的时候光标偏移
        self.text = tempPwdStr;
        for (UIView *v in self.rightView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                UIImageView *img = (UIImageView *)v;
                img.image=[UIImage imageNamed:@"眼睛闭着"];
            }
        }
    }
    
    self.secureTextEntry = !self.secureTextEntry;
}
-(void)setupUI{
    self.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.font = [UIFont systemFontOfSize:16];
    [self setValue:TPSPlaceholdColor forKeyPath:@"_placeholderLabel.textColor"];
    
    UIView *rightView=[[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width-20-40, 0, 30, 44)];
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"眼睛闭着"]];
    img.frame=CGRectMake(2.5, 17 , 26, 15);
    [rightView addSubview:img];
    
    UIButton *but=[UIButton buttonWithType:UIButtonTypeCustom];
    but.frame=CGRectMake(0, 0, 30, 44);
    [rightView addSubview:but];
    [but addTarget:self action:@selector(secureTextEntry) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightView = rightView;
    self.rightViewMode = UITextFieldViewModeAlways;
    self.secureTextEntry = YES;
    
//    [self setBorderStyle:UITextBorderStyleRoundedRect];
}
@end
