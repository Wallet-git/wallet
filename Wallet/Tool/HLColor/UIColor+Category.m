//
//  UIBarButtonItem+Item.m
//  LH
//
//  Created by 刘洪 on 13-8-5.
//  Copyright (c) 2013年 apple. All rights reserved.
//


#import "UIColor+Category.h"

@implementation UIColor (Category)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    
    return [UIColor colorWithHexString:hexString alpha:1.0f];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    if ([hexString length] <= 0)
        return nil;
    
    // Remove '#'
    if ([hexString hasPrefix:@"#"]) {
        hexString = [hexString substringFromIndex:1];
    }
    
    // Invalid if not 3, or 6 characters
    NSUInteger length = [hexString length];
    if (length != 3 && length != 6) {
        return nil;
    }
    
    NSUInteger digits = length / 3;
    CGFloat maxValue = ((digits == 1) ? 15.0 : 255.0);
    
    NSString *redString = [hexString substringWithRange:NSMakeRange(0, digits)];
    NSString *greenString = [hexString substringWithRange:NSMakeRange(digits, digits)];
    NSString *blueString = [hexString substringWithRange:NSMakeRange(2 * digits, digits)];
    
    int32_t red = 0;
    int32_t green = 0;
    int32_t blue = 0;
    
    sscanf([redString UTF8String], "%x", &red);
    sscanf([greenString UTF8String], "%x", &green);
    sscanf([blueString UTF8String], "%x", &blue);
    
    return [UIColor colorWithRed:red/maxValue green:green/maxValue blue:blue/maxValue alpha:alpha];
}

@end
