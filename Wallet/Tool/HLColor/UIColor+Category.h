//
//  UIBarButtonItem+Item.m
//  LH
//
//  Created by 刘洪 on 13-8-5.
//  Copyright (c) 2013年 apple. All rights reserved.
//


#import <UIKit/UIKit.h>
@interface UIColor (Category)

+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;

+ (UIColor *)colorWithHexString:(NSString *)hexString;


@end
