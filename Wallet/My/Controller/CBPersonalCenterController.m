//
//  PersonalCenterController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/8.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBPersonalCenterController.h"
#import "CBPersonalCenterModel.h"

#import "CBPersonalCenterCell.h"
#define kCBPersonalCenterCell @"CBPersonalCenterCell"

#import "CBPersonalCenterHeadCell.h"
#define kCBPersonalCenterHeadCell @"CBPersonalCenterHeadCell"

#import "CBSecurityCenterController.h"
#import "CBRuleWebController.h"
#import "CBJoinTeamController.h"
#import "CBGrievanceManController.h"
#import "CBLanguageController.h"

@interface CBPersonalCenterController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *dataSource;//!< 个人信息数据源
@property (nonatomic, strong) UITableView *infoTableView;
@end

@implementation CBPersonalCenterController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.infoTableView];
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 1;
    }else{
        return self.dataSource.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        CBPersonalCenterHeadCell *headCell = [tableView dequeueReusableCellWithIdentifier:kCBPersonalCenterHeadCell forIndexPath:indexPath];
        return headCell;

    }else{
        CBPersonalCenterCell *cell = [tableView dequeueReusableCellWithIdentifier:kCBPersonalCenterCell forIndexPath:indexPath];
        // 防止数组越界
        if (indexPath.row < self.dataSource.count) {
            CBPersonalCenterModel *model = self.dataSource[indexPath.row];
            cell.personalName.text = model.Name;
            cell.personalName.textColor = [UIColor colorWithHexString:@"#333333"];
            cell.personalImage.image = [UIImage imageNamed:model.Image];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 220;
    }else{
        return 50;
    }
}


- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CBPersonalCenterModel *model = self.dataSource[indexPath.row];
    NSInteger type = [model.Type integerValue];
    switch (type) {
        case CBSecurityCenter:
        {
            CBSecurityCenterController * security = [[CBSecurityCenterController alloc]init];
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:security];
            [self presentViewController:nav animated:YES completion:nil];
                
        }
            break;
        case CBRuleDescription:
        {
            CBRuleWebController * web = [[CBRuleWebController alloc]init];
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:web];
            [self presentViewController:nav animated:YES completion:nil];
            
        }
            break;
        case CBJoinTeam:
        {
            CBJoinTeamController * jion = [[CBJoinTeamController alloc]init];
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:jion];
            [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        case CBGrievanceManagement:
        {
            CBGrievanceManController * GrievanceMan = [[CBGrievanceManController alloc]init];
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:GrievanceMan];
            [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        case CBlanguageSettings:
        {
            CBLanguageController * language = [[CBLanguageController alloc]init];
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:language];
            [self presentViewController:nav animated:YES completion:nil];
        }
            break;
        case CBSharingFriends:
        {
            
        }
            break;
        case CBSystemNotification:
        {
            
        }
            break;
        case CBAboutUs:
        {
            
        }
            break;
        case CBSignOut:
        {
            
        }
            break;
            
        
    }
    
    
}



#pragma mark - 懒加载

- (NSMutableArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource = [[NSMutableArray alloc] init];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"UseInfoList" ofType:@"plist"];
        NSArray *arr = [NSMutableArray arrayWithContentsOfFile:path];
        for (NSDictionary *dic in arr) {
                CBPersonalCenterModel *model = [[CBPersonalCenterModel alloc] initWithDictionary:dic error:nil];
                [_dataSource addObject:model];
            
            }
    }
    return _dataSource;
}


- (UITableView *)infoTableView
{
    if (!_infoTableView) {
        _infoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _infoTableView.delegate = self;
        _infoTableView.dataSource = self;
        [_infoTableView registerNib:[UINib nibWithNibName:kCBPersonalCenterHeadCell bundle:nil] forCellReuseIdentifier:kCBPersonalCenterHeadCell];
        [_infoTableView registerNib:[UINib nibWithNibName:kCBPersonalCenterCell bundle:nil] forCellReuseIdentifier:kCBPersonalCenterCell];
        
        _infoTableView.tableFooterView = [[UIView alloc] init];
        _infoTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);

    }
    return _infoTableView;
}


@end
