//
//  CBJoinTeamController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBJoinTeamController.h"

@interface CBJoinTeamController ()
@property (weak, nonatomic) IBOutlet UILabel *teamNumber;
@property (weak, nonatomic) IBOutlet UITextField *teamNumberTextfield;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@end

@implementation CBJoinTeamController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addLeftBtn];
    [self greatUI];
}

- (void)greatUI{
    
    self.navigationItem.title = @"加入团队";
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F9F9FC"];
    self.teamNumber.text = @"请输入团队邀请码";
    self.teamNumber.textColor = [UIColor colorWithHexString:@"#6D778B"];
    
    [self.sureBtn setTitle:@"提交" forState:UIControlStateNormal];
    self.sureBtn.enabled = NO;
    self.sureBtn.backgroundColor = [UIColor colorWithHexString:@"#EBECEC"];
}



- (void)addLeftBtn {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
}

- (void)onClickedOKbtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changeTextfield:(UITextField *)sender {
    
    if (self.teamNumberTextfield.text.length == 0) {
        self.sureBtn.enabled = NO;
        self.sureBtn.backgroundColor = [UIColor colorWithHexString:@"#EBECEC"];
    }else{
        self.sureBtn.enabled = YES;
        [self.sureBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
        self.sureBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    }
    
}




@end
