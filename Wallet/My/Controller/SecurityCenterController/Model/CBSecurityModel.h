//
//  CBSecurityModel.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, CBSecurityListType) {
    CBRealNameCertification = 1,//实名认证
    CBBingIphone,
    CBBingEmail,
    CBPay,
    CBModifyPassWord
};



@interface CBSecurityModel : JSONModel
@property (nonatomic, copy) NSString * Name;
@property (nonatomic, copy) NSString * Image;
@property (nonatomic, copy) NSString * Type;

@end

NS_ASSUME_NONNULL_END
