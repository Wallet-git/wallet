//
//  CBBingTypeController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBBingTypeController.h"

@interface CBBingTypeController ()

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *iphoneLable;
@property (weak, nonatomic) IBOutlet UILabel *firstCodeLable;
@property (weak, nonatomic) IBOutlet UILabel *secondCodeLable;
@property (weak, nonatomic) IBOutlet UITextField *iphoneTextfield;
@property (weak, nonatomic) IBOutlet UITextField *firstCodeTextfield;
@property (weak, nonatomic) IBOutlet UITextField *secondTextfield;
@property (weak, nonatomic) IBOutlet UIButton *firstCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@property (weak, nonatomic) IBOutlet UIButton *thirdCodeBtn;
@property (weak, nonatomic) IBOutlet UILabel *itemLable;

@end

@implementation CBBingTypeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNav];
    [self greatUI];
}


- (void)greatUI{
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F9F9FC"];
    self.backgroundView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    
    [self.firstCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.firstCodeBtn setTitleColor:[UIColor colorWithHexString:@"#3B67E0"] forState:UIControlStateNormal];
    [self.secondCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.secondCodeBtn setTitleColor:[UIColor colorWithHexString:@"#3B67E0"] forState:UIControlStateNormal];
    [self.thirdCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.thirdCodeBtn setTitleColor:[UIColor colorWithHexString:@"#3B67E0"] forState:UIControlStateNormal];
    
    [self.sureBtn setTitle:@"确认" forState:UIControlStateNormal];
    [self.sureBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.sureBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    
    self.firstCodeBtn.layer.borderColor = [[UIColor colorWithHexString:@"#3B67E0"] CGColor];
    self.firstCodeBtn.layer.borderWidth = 1.0f;
    self.firstCodeBtn.layer.cornerRadius = 12.0f;
    
    self.secondCodeBtn.layer.borderColor = [[UIColor colorWithHexString:@"#3B67E0"] CGColor];
    self.secondCodeBtn.layer.borderWidth = 1.0f;
    self.secondCodeBtn.layer.cornerRadius = 12.0f;
    
    self.thirdCodeBtn.layer.borderColor = [[UIColor colorWithHexString:@"#3B67E0"] CGColor];
    self.thirdCodeBtn.layer.borderWidth = 1.0f;
    self.thirdCodeBtn.layer.cornerRadius = 12.0f;
    
    [self bingType];//判断类型
}

- (void)setupNav{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
    
}

- (void)bingType{
    
    if ([self.tpye isEqualToString:@"Iphone"]) {
        self.title = @"绑定手机号";
        self.iphoneLable.text = @"手机号";
        self.firstCodeLable.text = @"验证码";
        self.secondCodeLable.text = @"邮箱验证";
        self.thirdCodeBtn.alpha = 0;
        self.itemLable.alpha = 0;        
        self.iphoneTextfield.placeholder = @"请输入您的手机号";
        self.firstCodeTextfield.placeholder = @"请输入验证码";
        self.secondTextfield.placeholder = @"请输入验证码";
        
    }else if ([self.tpye isEqualToString:@"Email"]){
        self.title = @"绑定邮箱";
        self.iphoneLable.text = @"邮箱账号";
        self.firstCodeLable.text = @"邮箱验证";
        self.secondCodeLable.text = @"短信验证";
        self.thirdCodeBtn.alpha = 0;
        self.itemLable.alpha = 0;
        self.iphoneTextfield.placeholder = @"请输入您的邮箱号";
        self.firstCodeTextfield.placeholder = @"请输入验证码";
        self.secondTextfield.placeholder = @"请输入验证码";
    }else{
        self.title = @"修改密码";
        self.iphoneLable.text = @"验证码";
        self.firstCodeLable.text = @"新密码";
        self.secondCodeLable.text = @"确认密码";
        self.thirdCodeBtn.alpha = 1;
        self.itemLable.alpha = 1;
        self.iphoneTextfield.placeholder = @"请输入验证码";
        self.firstCodeTextfield.placeholder = @"请输入密码";
        self.secondTextfield.placeholder = @"请再次输入密码";
    }
    
}



- (void)onClickedOKbtn {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
