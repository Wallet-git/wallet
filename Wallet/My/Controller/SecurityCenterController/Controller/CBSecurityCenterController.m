//
//  CBSecurityCenterController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBSecurityCenterController.h"
#import "CBSecurityModel.h"

#import "CBSecurityCell.h"
#define kCBSecurityCell @"CBSecurityCell"
#import "CBSecurityHeadCell.h"
#define kCBSecurityHeadCell @"CBSecurityHeadCell"

#import "CBBingTypeController.h"

@interface CBSecurityCenterController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UITableView * securityTableView;
@end

@implementation CBSecurityCenterController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"安全中心";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.securityTableView];
    
    [self addLeftBtn];
}

- (void)viewWillAppear:(BOOL)animated{

    //设置导航栏背景图片为一个空的image，这样就透明了
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    //去掉透明后导航栏下边的黑边
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
}
- (void)viewWillDisappear:(BOOL)animated{

    //如果不想让其他页面的导航栏变为透明 需要重置
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}


- (void)addLeftBtn {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};

}

- (void)onClickedOKbtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 1;
    }else{
        return self.dataSource.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {

        CBSecurityHeadCell *headCell = [tableView dequeueReusableCellWithIdentifier:kCBSecurityHeadCell forIndexPath:indexPath];
        return headCell;

    }else{
        CBSecurityCell *cell = [tableView dequeueReusableCellWithIdentifier:kCBSecurityCell forIndexPath:indexPath];
        // 防止数组越界
        if (indexPath.row < self.dataSource.count) {
            CBSecurityModel *model = self.dataSource[indexPath.row];
            cell.SecurityName.text = model.Name;
            cell.SecurityName.textColor = [UIColor colorWithHexString:@"#333333"];
            cell.SecurityImage.image = [UIImage imageNamed:model.Image];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 220;
    }else{
        return 50;
    }
}


- (BOOL) tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return NO;
    }
    return YES;
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CBSecurityModel *model = self.dataSource[indexPath.row];
    NSInteger type = [model.Type integerValue];
    switch (type) {
        case CBRealNameCertification:
        {
            
        }
            break;
        case CBBingIphone:
        {
            CBBingTypeController * bing = [[CBBingTypeController alloc]init];
            bing.tpye = @"Iphone";
            [self.navigationController pushViewController:bing animated:YES];
        }
            break;
        case CBBingEmail:
        {
            CBBingTypeController * bing = [[CBBingTypeController alloc]init];
            bing.tpye = @"Email";
            [self.navigationController pushViewController:bing animated:YES];
        }
            break;
        case CBPay:
        {
            
        }
            break;
        case CBModifyPassWord:
        {
            CBBingTypeController * bing = [[CBBingTypeController alloc]init];
            bing.tpye = @"PassWord";
            [self.navigationController pushViewController:bing animated:YES];
        }
            break;
    }
    
    
}



#pragma mark - 懒加载

- (NSMutableArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource = [[NSMutableArray alloc] init];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"SecurityCenterInfoList" ofType:@"plist"];
        NSArray *arr = [NSMutableArray arrayWithContentsOfFile:path];
        for (NSDictionary *dic in arr) {
            CBSecurityModel *model = [[CBSecurityModel alloc] initWithDictionary:dic error:nil];
            [_dataSource addObject:model];
            
        }
    }
    return _dataSource;
}


- (UITableView *)securityTableView
{
    if (!_securityTableView) {
        _securityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _securityTableView.delegate = self;
        _securityTableView.dataSource = self;
        [_securityTableView registerNib:[UINib nibWithNibName:kCBSecurityCell bundle:nil] forCellReuseIdentifier:kCBSecurityCell];
        [_securityTableView registerNib:[UINib nibWithNibName:kCBSecurityHeadCell bundle:nil] forCellReuseIdentifier:kCBSecurityHeadCell];
        _securityTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _securityTableView.tableFooterView = [[UIView alloc] init];
        _securityTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }
    return _securityTableView;
}




@end
