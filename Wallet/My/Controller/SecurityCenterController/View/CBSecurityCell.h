//
//  CBSecurityCell.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBSecurityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *SecurityImage;
@property (weak, nonatomic) IBOutlet UILabel *SecurityName;
@property (weak, nonatomic) IBOutlet UILabel *auxiliaryName;

@end

NS_ASSUME_NONNULL_END
