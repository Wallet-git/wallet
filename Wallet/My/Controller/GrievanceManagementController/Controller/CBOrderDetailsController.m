//
//  CBOrderDetailsController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBOrderDetailsController.h"

#import "CBOrderTopCell.h"
#define kCBOrderTopCell @"CBOrderTopCell"

#import "CBOrderMidCell.h"
#define kCBOrderMidCell @"CBOrderMidCell"

#import "CBOrderBottonCell.h"
#define kCBOrderBottonCell @"CBOrderBottonCell"

@interface CBOrderDetailsController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * OderTableView;

@end

@implementation CBOrderDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"申诉管理";
    [self.view addSubview:self.OderTableView];    
    [self addLeftBtn];
}

- (void)addLeftBtn {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
    
}

- (void)onClickedOKbtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 6;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        CBOrderTopCell *topCell = [tableView dequeueReusableCellWithIdentifier:kCBOrderTopCell forIndexPath:indexPath];
        return topCell;
    }else if (indexPath.section == 1){
        CBOrderMidCell *midCell = [tableView dequeueReusableCellWithIdentifier:kCBOrderMidCell forIndexPath:indexPath];
        return midCell;
    }else{
        CBOrderBottonCell *bottonCell = [tableView dequeueReusableCellWithIdentifier:kCBOrderBottonCell forIndexPath:indexPath];
        return bottonCell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 280;
    }else if (indexPath.section == 2){
        return 243;
    }else{
        return 40;
    }
}



#pragma mark 懒加载

- (UITableView *)OderTableView
{
    if (!_OderTableView) {
        _OderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _OderTableView.delegate = self;
        _OderTableView.dataSource = self;
        [_OderTableView registerNib:[UINib nibWithNibName:kCBOrderTopCell bundle:nil] forCellReuseIdentifier:kCBOrderTopCell];
        [_OderTableView registerNib:[UINib nibWithNibName:kCBOrderMidCell bundle:nil] forCellReuseIdentifier:kCBOrderMidCell];
        [_OderTableView registerNib:[UINib nibWithNibName:kCBOrderBottonCell bundle:nil] forCellReuseIdentifier:kCBOrderBottonCell];
        _OderTableView.tableFooterView = [[UIView alloc] init];
        _OderTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return _OderTableView;
}

@end
