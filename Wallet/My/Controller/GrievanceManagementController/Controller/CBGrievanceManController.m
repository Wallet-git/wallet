//
//  CBGrievanceManController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBGrievanceManController.h"

#import "CBGrievanceManCell.h"
#define kCBGrievanceManCell @"CBGrievanceManCell"

#import "CBOrderDetailsController.h"

@interface CBGrievanceManController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView * grievanceManTableView;
@end

@implementation CBGrievanceManController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"申诉管理";
    self.grievanceManTableView.backgroundColor = [UIColor colorWithHexString:@"#F9F9FC"];
    [self.view addSubview:self.grievanceManTableView];
    
    [self addLeftBtn];
}

- (void)addLeftBtn {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
    
}

- (void)onClickedOKbtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CBGrievanceManCell *headCell = [tableView dequeueReusableCellWithIdentifier:kCBGrievanceManCell forIndexPath:indexPath];
    
    return headCell;

}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 80;
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    CBOrderDetailsController * order = [[CBOrderDetailsController alloc]init];
    [self.navigationController pushViewController:order animated:YES];
}



#pragma mark 懒加载

- (UITableView *)grievanceManTableView
{
    if (!_grievanceManTableView) {
        _grievanceManTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _grievanceManTableView.delegate = self;
        _grievanceManTableView.dataSource = self;
        [_grievanceManTableView registerNib:[UINib nibWithNibName:kCBGrievanceManCell bundle:nil] forCellReuseIdentifier:kCBGrievanceManCell];
        _grievanceManTableView.tableFooterView = [[UIView alloc] init];
        _grievanceManTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
    }
    return _grievanceManTableView;
}


@end
