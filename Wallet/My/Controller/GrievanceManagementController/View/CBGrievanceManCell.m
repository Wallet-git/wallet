//
//  CBGrievanceManCell.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBGrievanceManCell.h"

@implementation CBGrievanceManCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor colorWithHexString:@"#F9F9FC"];
    self.backGroundView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    self.numberLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
    self.orderLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
    
    self.timeLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
