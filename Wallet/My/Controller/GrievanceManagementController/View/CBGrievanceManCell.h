//
//  CBGrievanceManCell.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBGrievanceManCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backGroundView;
@property (weak, nonatomic) IBOutlet UILabel *numberLable;
@property (weak, nonatomic) IBOutlet UILabel *orderLable;
@property (weak, nonatomic) IBOutlet UILabel *TypeLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;

@end

NS_ASSUME_NONNULL_END
