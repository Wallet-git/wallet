//
//  CBLanguageController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBLanguageController.h"

@interface CBLanguageController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray * dataSource;
@property (nonatomic, strong) UITableView * languageTableView;
@end

@implementation CBLanguageController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"语言";
    [self.view addSubview:self.languageTableView];
    [self addLeftBtn];
}

- (void)addLeftBtn {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:nil style:UIBarButtonItemStylePlain target:self action:@selector(onClickedOKbtn)];
    [self.navigationItem.leftBarButtonItem setImage:[UIImage imageNamed:@"SS返回"]];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont systemFontOfSize:18]};
    
}

- (void)onClickedOKbtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.textColor = [UIColor colorWithHexString:@"#9299A7"];
    switch (indexPath.row) {
        case 0:{
            cell.textLabel.text = @"默认";
        }
            break;
        case 1:{
            cell.textLabel.text = @"简体中文";
        }
            break;
        case 2:{
            cell.textLabel.text = @"繁体中文";
        }
            break;
        case 3:{
            cell.textLabel.text = @"English";
        }
            break;
    }  
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UITableView *)languageTableView
{
    if (!_languageTableView) {
        _languageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
        _languageTableView.delegate = self;
        _languageTableView.dataSource = self;

        _languageTableView.tableFooterView = [[UIView alloc] init];
        _languageTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return _languageTableView;
}

@end
