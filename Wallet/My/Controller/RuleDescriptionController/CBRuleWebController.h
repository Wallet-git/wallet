//
//  CBRuleWebController.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBRuleWebController : UIViewController
@property (nonatomic,strong)NSString *urlString;
@property (nonatomic,strong)NSString *name;
@end

NS_ASSUME_NONNULL_END
