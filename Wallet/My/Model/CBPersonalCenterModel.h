//
//  CBPersonalCenterModel.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, CBPersonalCenterListType) {
    CBSecurityCenter = 1,//安全中心
    CBRuleDescription,
    CBJoinTeam,
    CBGrievanceManagement,
    CBlanguageSettings,
    CBSharingFriends,
    CBSystemNotification,
    CBAboutUs,
    CBSignOut
};


@interface CBPersonalCenterModel : JSONModel
@property (nonatomic, copy) NSString * Name;
@property (nonatomic, copy) NSString * Image;
@property (nonatomic, copy) NSString * Type;
@end

NS_ASSUME_NONNULL_END
