//
//  CBPersonalCenterHeadCell.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBPersonalCenterHeadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *text;
@property (weak, nonatomic) IBOutlet UIImageView *midViewImage;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *UIDLable;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage;
@property (weak, nonatomic) IBOutlet UILabel *serivceLable;
@property (weak, nonatomic) IBOutlet UIButton *moneyBtn;

@property (weak, nonatomic) IBOutlet UIButton *realNameBtn;
@property (weak, nonatomic) IBOutlet UIImageView *realNameImage;
@property (weak, nonatomic) IBOutlet UIImageView *levelImage;

@end

NS_ASSUME_NONNULL_END
