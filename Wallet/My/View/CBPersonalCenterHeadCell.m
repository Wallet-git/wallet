//
//  CBPersonalCenterHeadCell.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBPersonalCenterHeadCell.h"

@implementation CBPersonalCenterHeadCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.midViewImage.layer.borderColor = [[UIColor clearColor] CGColor];
    self.midViewImage.layer.borderWidth = 1.0f;
    self.midViewImage.layer.cornerRadius = 5.0f;
    self.moneyBtn.layer.borderColor = [[UIColor clearColor] CGColor];
    self.moneyBtn.layer.borderWidth = 1.0f;
    self.moneyBtn.layer.cornerRadius = 20.0f;
    self.realNameBtn.layer.borderColor = [[UIColor clearColor] CGColor];
    self.realNameBtn.layer.borderWidth = 1.0f;
    self.realNameBtn.layer.cornerRadius = 20.0f;
    
    self.nameLable.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    self.UIDLable.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    
    [self.moneyBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    [self.realNameBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
    
    self.moneyBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.moneyBtn.layer.shadowOffset = CGSizeMake(0,0);
    self.moneyBtn.layer.shadowOpacity = 0.5;
    self.moneyBtn.layer.shadowRadius = 2;
    
    self.realNameBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.realNameBtn.layer.shadowOffset = CGSizeMake(0,0);
    self.realNameBtn.layer.shadowOpacity = 0.5;
    self.realNameBtn.layer.shadowRadius = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)MoneyBTN:(UIButton *)sender {
    
    
}

- (IBAction)realNameBTN:(UIButton *)sender {
    
    
}


@end
