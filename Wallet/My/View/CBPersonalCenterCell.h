//
//  CBPersonalCenterCell.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/11.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBPersonalCenterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *personalImage;
@property (weak, nonatomic) IBOutlet UILabel *personalName;

@end

NS_ASSUME_NONNULL_END
