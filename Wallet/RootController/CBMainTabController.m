//
//  CBMainTabController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/8.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBMainTabController.h"
#import "CBNavController.h"
#import "CBContactViewController.h"
#import "CBTeamController.h"
#import "CBWalletBagController.h"
#import "CBPersonalCenterController.h"

@interface CBMainTabController ()

@end

@implementation CBMainTabController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpAllChildViewController];
}

- (void)setUpAllChildViewController{
    
    CBContactViewController *oneVC = [[CBContactViewController alloc]init];
    [self setUpOneChildViewController:oneVC image:[UIImage imageNamed:@"接单---"] title:@"接单" selectImage:[UIImage imageNamed:@"接单"]];
    
    CBTeamController *twoVC = [[CBTeamController alloc]init];
    [self setUpOneChildViewController:twoVC image:[UIImage imageNamed:@"钱包---"] title:@"团队" selectImage:[UIImage imageNamed:@"钱包"]];
    
    CBWalletBagController *threeVC = [[CBWalletBagController alloc]init];
    [self setUpOneChildViewController:threeVC image:[UIImage imageNamed:@"团队---"] title:@"钱包" selectImage:[UIImage imageNamed:@"团队"]];
    
    CBPersonalCenterController *fourVC = [[CBPersonalCenterController alloc]init];
    [self setUpOneChildViewController:fourVC image:[UIImage imageNamed:@"我的---"] title:@"我的" selectImage:[UIImage imageNamed:@"我的"]];
}



- (void)setUpOneChildViewController:(UIViewController *)viewController image:(UIImage *)image title:(NSString *)title selectImage: (UIImage *)select{
    
    viewController.title = title;
    viewController.tabBarItem.image =  [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    viewController.tabBarItem.selectedImage = [select imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    viewController.navigationItem.title = title;
      
    [self addChildViewController:viewController];

}



@end
