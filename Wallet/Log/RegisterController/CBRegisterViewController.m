//
//  CBRegisterViewController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBRegisterViewController.h"
#import "CBPaymentBindingController.h"

@interface CBRegisterViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *bigView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollviewHeight;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UIButton *phoneLable;
@property (weak, nonatomic) IBOutlet UIButton *emailLable;
@property (weak, nonatomic) IBOutlet UIView *phoneLine;
@property (weak, nonatomic) IBOutlet UIView *emailLine;
@property (weak, nonatomic) IBOutlet UIButton *numberPlace;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfield;
@property (weak, nonatomic) IBOutlet UIView *phoneBottonLine;
@property (weak, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIView *codeBottonLine;
@property (weak, nonatomic) IBOutlet UITextFieldCiphertext *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIView *passwordBottonLine;
@property (weak, nonatomic) IBOutlet UITextField *remmendCodeTextfield;
@property (weak, nonatomic) IBOutlet UIView *remmendBottonLine;
@property (weak, nonatomic) IBOutlet UIButton *registBtn;
@property (weak, nonatomic) IBOutlet UIView *middleView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
@property (nonatomic,copy) NSString * type;//注册方式

@property (weak, nonatomic) IBOutlet UILabel *errorLable;



@end

@implementation CBRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * tapGesturRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    [self.bigView addGestureRecognizer:tapGesturRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registTextFieldTextChange) name:UITextFieldTextDidChangeNotification object:nil];
    
    [self greatUI];
}

- (void)registTextFieldTextChange
{    
    if ([self.type isEqualToString:@"Ipnone"]) {
        
        if (self.phoneTextfield.text.length > 0 && self.passwordTextfield.text.length > 0 && self.codeTextfield.text.length) {
            self.registBtn.enabled = YES;
            [self.registBtn setImage:[UIImage imageNamed:@"按钮--可点击状态"] forState:UIControlStateNormal];
        }else{
            self.registBtn.enabled = NO;
            self.errorLable.alpha = 0;
            [self.registBtn setImage:[UIImage imageNamed:@"按钮--不可点击"] forState:UIControlStateNormal];
        }
    }else{
        if (self.emailTextfield.text.length > 0 && self.passwordTextfield.text.length > 0 && self.codeTextfield.text.length) {
            self.registBtn.enabled = YES;
            [self.registBtn setImage:[UIImage imageNamed:@"按钮--可点击状态"] forState:UIControlStateNormal];
        }else{
            self.registBtn.enabled = NO;
            self.errorLable.alpha = 0;
            [self.registBtn setImage:[UIImage imageNamed:@"按钮--不可点击"] forState:UIControlStateNormal];
        }
    }
}


-(void)tapAction:(id)tap{
    
    [self.bigView endEditing:YES];
}

- (void)greatUI{
    self.titleLable.text = @"注册";
    self.type = @"Ipnone";//默认选择手机注册
    self.scrollviewHeight.constant = 700;
    self.bigView.backgroundColor = [UIColor colorWithHexString:@"#111622"];
    self.titleLable.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#111622"];
    self.middleView.backgroundColor = [UIColor colorWithHexString:@"#1A202E"];
    self.middleView.backgroundColor = [UIColor colorWithHexString:@"#1A202E"];
    
    [self.phoneLable setTitle:@"手机号" forState:UIControlStateNormal];
    [self.phoneLable setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    [self.emailLable setTitle:@"邮箱" forState:UIControlStateNormal];
    [self.emailLable setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    
    [self.numberPlace setTitle:@"+86 " forState:UIControlStateNormal];
    [self.numberPlace setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    
    [self.codeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
    [self.codeBtn setTitleColor:[UIColor colorWithHexString:@"#0078FF"] forState:UIControlStateNormal];
    
    self.phoneBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.codeBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.passwordBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.remmendBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    
    self.phoneLine.backgroundColor = [UIColor colorWithHexString:@"#0078FF"];
    self.emailLine.backgroundColor = [UIColor colorWithHexString:@"#0078FF"];
    self.emailLine.alpha = 0;
    self.emailTextfield.alpha = 0;
    self.errorLable.alpha = 0;
    self.registBtn.enabled = NO;
    
    self.phoneTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    
    self.codeTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"短信验证码" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    
    self.passwordTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"密码（6-20字符）" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    
    self.remmendCodeTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"团队推荐码（选填）" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    
    self.emailTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"邮箱地址" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    
    self.phoneTextfield.delegate = self;
    self.codeTextfield.delegate = self;
    self.passwordTextfield.delegate = self;
    //self.remmendCodeTextfield.delegate = self;
    self.emailTextfield.delegate = self;
    
}

- (IBAction)phoneBTN:(UIButton *)sender {
    
    self.emailLine.alpha = 0;
    self.emailTextfield.alpha = 0;
    self.phoneLine.alpha = 1;
    self.numberPlace.alpha = 1;
    self.phoneTextfield.alpha = 1;
    
    self.type = @"Ipnone";
}


- (IBAction)emailBTN:(UIButton *)sender {
    
    self.emailLine.alpha = 1;
    self.emailTextfield.alpha = 1;
    self.phoneLine.alpha = 0;
    self.phoneTextfield.alpha = 0;
    self.numberPlace.alpha = 0;
    
    self.type = @"Email";
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

//点击return 按钮 去掉
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)backBtn:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark 注册
- (IBAction)registBTN:(UIButton *)sender {
    
    TpsRequestDataFromServer *requestManager = [TpsRequestDataFromServer shareManager];
//    requestManager.timeoutInterval = 5;//!< 设置请求响应时间
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                self.passwordTextfield.text, @"password",
                                @(2),@"type",
                                self.codeTextfield.text,@"code",
                                @"1234",@"app-name",
                                @"",@"time",
                                nil];
    if ([self.type isEqualToString:@"Ipnone"]) {
        [parameters setObject:self.phoneTextfield.text forKey:@"mobile"];
    }else{
        [parameters setObject:self.emailTextfield.text forKey:@"email"];
    }

    NSString *url = [NSString stringWithFormat:@"%@/api/login/register",API];
    [requestManager requestFromServerWithUrl:url parameters:parameters result:^(NSDictionary *json, NSError *error) {
        if (error == nil) {
            
            NSString *code = [NSString stringWithFormat:@"%@",[json objectForKey:@"code"]];
            if ([code isEqualToString:@"10000"]) {
                NSDictionary * data = json[@"data"];
                NSString * list = [data objectForKey:@"list"];
                //CBUserManager *manager = [[CBUserManager shareManager] initWithDictionary:data error:&error];
                [CBUserManager shareManager].list = list;
                
                CBPaymentBindingController * payment = [[CBPaymentBindingController alloc]init];
                [self presentViewController:payment animated:YES completion:nil];
                
            }else{
                
                self.errorLable.alpha = 1;
                self.errorLable.text = [NSString stringWithFormat:@"%@",json[@"code"]];
                self.errorLable.textColor = [UIColor colorWithHexString:@"#FF3750"];
            }
        }else{
            NSLog(@"%@",error);
        }
        
    }];
}


@end
