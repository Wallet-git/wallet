//
//  CBUserManager.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/12.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBUserManager.h"

@implementation CBUserManager

+ (instancetype)shareManager
{
    static CBUserManager *user;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[CBUserManager alloc] init];
    });
    return user;
}

- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

+ (BOOL) propertyIsOptional:(NSString *)propertyName
{
    return YES;
}


@end
