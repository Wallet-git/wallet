//
//  CBForgetPWController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBForgetPWController.h"

@interface CBForgetPWController ()
@property (weak, nonatomic) IBOutlet UILabel *passwordLable;
@property (weak, nonatomic) IBOutlet UIView *RetrievePWView;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *emailBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumberBtn;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextfiled;
@property (weak, nonatomic) IBOutlet UITextField *codeTextfield;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIView *phoneLine;
@property (weak, nonatomic) IBOutlet UIView *emileLine;
@property (weak, nonatomic) IBOutlet UIView *phoneBottonLine;
@property (weak, nonatomic) IBOutlet UIView *codeBottonLine;
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;

@property (weak, nonatomic) IBOutlet UIView *RetrieveSuccessView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UITextField *againPWTextfield;
@property (weak, nonatomic) IBOutlet UIView *PWLine;
@property (weak, nonatomic) IBOutlet UIView *againPWLine;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;

@end

@implementation CBForgetPWController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self greatUI];
}

- (void)greatUI{
    
    self.passwordLable.text = @"找回密码";
    self.passwordLable.textColor = [UIColor colorWithHexString:@"#FFFFFF"];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#111622"];
    self.RetrievePWView.backgroundColor = [UIColor colorWithHexString:@"#1A202E"];
    [self.phoneBtn setTitle:@"手机号" forState:UIControlStateNormal];
    [self.phoneBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    [self.emailBtn setTitle:@"邮箱" forState:UIControlStateNormal];
    [self.emailBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    [self.phoneNumberBtn setTitle:@"+86 " forState:UIControlStateNormal];
    [self.phoneNumberBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    [self.codeBtn setTitle:@"重新发送" forState:UIControlStateNormal];
    [self.codeBtn setTitleColor:[UIColor colorWithHexString:@"#0078FF"] forState:UIControlStateNormal];
    self.phoneTextfiled.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"请输入手机号" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    self.codeTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"短信验证码" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    self.emailTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"邮箱地址" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    self.phoneBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.codeBottonLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.phoneLine.backgroundColor = [UIColor colorWithHexString:@"#0078FF"];
    self.emileLine.backgroundColor = [UIColor colorWithHexString:@"#0078FF"];
    self.emileLine.alpha = 0;
    self.emailTextfield.alpha = 0;
    
    [self nextView];
}

- (void)nextView{
    
    self.RetrieveSuccessView.alpha = 0;
    self.RetrieveSuccessView.backgroundColor = [UIColor colorWithHexString:@"#1A202E"];
    self.PWLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.passwordTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"设置6-20位密码" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    self.againPWTextfield.attributedPlaceholder= [[NSAttributedString alloc]initWithString:@"请再次输入密码" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#6D778B"]}];
    self.PWLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.againPWLine.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
}


- (IBAction)nextBtn:(UIButton *)sender {
    self.RetrievePWView.alpha = 0;
    self.RetrieveSuccessView.alpha = 1;
}





- (IBAction)phoneBTN:(UIButton *)sender {
    self.phoneLine.alpha = 1;
    self.emileLine.alpha = 0;
    self.phoneNumberBtn.alpha = 1;
    self.phoneTextfiled.alpha = 1;
    self.emailTextfield.alpha = 0;
}


- (IBAction)emailBTN:(UIButton *)sender {
    self.phoneLine.alpha = 0;
    self.emileLine.alpha = 1;
    self.phoneNumberBtn.alpha = 0;
    self.phoneTextfiled.alpha = 0;
    self.emailTextfield.alpha = 1;
}



- (IBAction)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)modifySuccessPWBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

@end
