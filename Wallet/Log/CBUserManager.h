//
//  CBUserManager.h
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/12.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBUserManager : JSONModel
@property (nonatomic, copy) NSString *list; 
+ (instancetype)shareManager;
@end

NS_ASSUME_NONNULL_END
