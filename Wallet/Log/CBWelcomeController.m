//
//  CBWelcomeController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/8.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBWelcomeController.h"
#import "CBLogViewController.h"
#import "CBRegisterViewController.h"


@interface CBWelcomeController ()
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UIButton *logButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation CBWelcomeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IPHONE_X) {
        self.background.image = [UIImage imageNamed:@"iPhone XS"];
    }else if (IS_IPHONE_Xr){
        self.background.image = [UIImage imageNamed:@"iPhone XR"];
    }else if (IS_IPHONE_Xs_Max){
        self.background.image = [UIImage imageNamed:@"iPhone XS Max"];
    }else{
        self.background.image = [UIImage imageNamed:@"欢迎界面"];
    }
    
    [self GreatUI];
    
    
}

- (void)GreatUI{
    
    [self.logButton setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.logButton.layer.borderColor = [[UIColor clearColor] CGColor];
    self.logButton.layer.borderWidth = 1.0f;
    self.logButton.layer.cornerRadius = 26.0f;
    
    [self.registerButton setTitleColor:[UIColor colorWithHexString:@"#0078FF"] forState:UIControlStateNormal];
    self.registerButton.layer.borderColor = [[UIColor clearColor] CGColor];
    self.registerButton.layer.borderWidth = 1.0f;
    self.registerButton.layer.cornerRadius = 26.0f;
}

- (IBAction)logBtn:(UIButton *)sender {
    
    CBLogViewController * log = [[CBLogViewController alloc]init];
    [self presentViewController:log animated:YES completion:nil];
    
}

- (IBAction)registerBtn:(UIButton *)sender {
    
    CBRegisterViewController * reg = [[CBRegisterViewController alloc]init];
    [self presentViewController:reg animated:YES completion:nil];
}



@end
