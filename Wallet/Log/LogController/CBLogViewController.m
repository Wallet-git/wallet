//
//  CBLogViewController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/8.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBLogViewController.h"
#import "CBForgetPWController.h"
#import "CBPaymentBindingController.h"
#import "CBRegisterViewController.h"
@interface CBLogViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *middleView;
@property (weak, nonatomic) IBOutlet UILabel *useNameLable;
@property (weak, nonatomic) IBOutlet UILabel *passWordLable;

@property (weak, nonatomic) IBOutlet UITextField *uesNameTextfild;
@property (weak, nonatomic) IBOutlet UITextField *passWordTextfild;

@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;
@property (weak, nonatomic) IBOutlet UIButton *Item;

@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UILabel *logLable;
@property (weak, nonatomic) IBOutlet UIView *lineOne;
@property (weak, nonatomic) IBOutlet UIView *lineTwo;
@property (weak, nonatomic) IBOutlet UIButton *delectUseBtn;
@property (weak, nonatomic) IBOutlet UIButton *delectPWBtn;
@property (weak, nonatomic) IBOutlet UIButton *logBtn;
@property (weak, nonatomic) IBOutlet UILabel *errorLable;

@end

@implementation CBLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginTextFieldTextChange) name:UITextFieldTextDidChangeNotification object:nil];
    
    [self greatUI];
    
}


- (void)loginTextFieldTextChange
{
    if (self.uesNameTextfild.text.length > 0 && self.passWordTextfild.text.length > 0) {
        self.logBtn.enabled = YES;
        [self.logBtn setImage:[UIImage imageNamed:@"按钮--可点击状态"] forState:UIControlStateNormal];
    }else{
        self.logBtn.enabled = NO;
        self.lineOne.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
        self.lineTwo.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
        self.errorLable.alpha = 0;
        [self.logBtn setBackgroundImage:[UIImage imageNamed:@"按钮--不可点击状态"] forState:UIControlStateNormal];
    }
}

- (void)greatUI{
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#111622"];
    self.middleView.backgroundColor = [UIColor colorWithHexString:@"#1A202E"];
    [self.registerBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    
    self.useNameLable.text = @"邮箱/手机号";
    self.useNameLable.textColor = [UIColor colorWithHexString:@"#0078FF"];
    self.passWordLable.text = @"密码";
    self.passWordLable.textColor = [UIColor colorWithHexString:@"#0078FF"];
    [self.forgetBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [self.forgetBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];   
    [self.Item setTitle:@"条款及隐私政策" forState:UIControlStateNormal];
    [self.Item setTitleColor:[UIColor colorWithHexString:@"#6D778B"] forState:UIControlStateNormal];
    [self.logBtn setBackgroundImage:[UIImage imageNamed:@"按钮--不可点击状态"] forState:UIControlStateNormal];
    self.lineOne.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    self.lineTwo.backgroundColor = [UIColor colorWithHexString:@"#2E3546"];
    
    self.delectUseBtn.alpha = 0;
    self.delectPWBtn.alpha = 0;
    self.errorLable.alpha = 0;
    self.logBtn.enabled = NO;
    self.uesNameTextfild.delegate = self;
    self.passWordTextfild.delegate = self;
}


#pragma mark 输入框
- (IBAction)changeTextfild:(UITextField *)sender {
    if (self.uesNameTextfild.text.length == 0) {
        self.delectUseBtn.alpha = 0;
    }else{
        self.delectUseBtn.alpha = 1;
    }
}

- (IBAction)endTextfild:(UITextField *)sender {    
    self.delectUseBtn.alpha = 0;
}

- (IBAction)passwordChange:(UITextField *)sender {
    if (self.passWordTextfild.text.length == 0) {
        self.delectPWBtn.alpha = 0;
    }else{
        self.delectPWBtn.alpha = 1;
    }
}

- (IBAction)passwordEndChange:(UITextField *)sender {
    self.delectPWBtn.alpha = 0;
}

- (IBAction)clearDelectBtn:(UIButton *)sender {
    if (self.uesNameTextfild.text.length > 0) {
        self.uesNameTextfild.text = nil;
        self.delectUseBtn.alpha = 0;
    }
}

- (IBAction)clearPwBtn:(UIButton *)sender {
    if(self.passWordTextfild.text.length > 0){
        self.passWordTextfild.text = nil;
        self.delectPWBtn.alpha = 0;
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)backBtn:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)registerBtn:(UIButton *)sender {
    
    CBRegisterViewController * reg = [[CBRegisterViewController alloc]init];
    [self presentViewController:reg animated:YES completion:nil];
    
}

- (IBAction)LogBtn:(UIButton *)sender {
    
    TpsRequestDataFromServer *requestManager = [TpsRequestDataFromServer shareManager];
    //    requestManager.timeoutInterval = 5;//!< 设置请求响应时间
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.passWordTextfild.text, @"password",
                                       self.uesNameTextfild.text, @"account",
                                       @(2),@"type",
                                       @"1234",@"app-name",
                                       @"",@"time",
                                       nil];
    
    NSString *url = [NSString stringWithFormat:@"%@/api/login/login",API];
    [requestManager requestFromServerWithUrl:url parameters:parameters result:^(NSDictionary *json, NSError *error) {
        if (error == nil) {
            NSString *code = [NSString stringWithFormat:@"%@",[json objectForKey:@"code"]];
            if ([code isEqualToString:@"10000"]) {
                NSDictionary * data = json[@"data"];
                NSString * list = [data objectForKey:@"list"];
                [CBUserManager shareManager].list = list;
                
                CBPaymentBindingController * payment = [[CBPaymentBindingController alloc]init];
                [self presentViewController:payment animated:YES completion:nil];
            }else{

                self.errorLable.alpha = 1;
                self.errorLable.text = [NSString stringWithFormat:@"%@",json[@"msg"]];
                self.errorLable.textColor = [UIColor colorWithHexString:@"#FF3750"];
                self.lineOne.backgroundColor = [UIColor colorWithHexString:@"#FF3750"];
                self.lineTwo.backgroundColor = [UIColor colorWithHexString:@"#FF3750"];
            }
        }else{
            NSLog(@"%@",error);
        }
        
    }];
    
}



- (IBAction)forgetBtn:(UIButton *)sender {
    CBForgetPWController * forget = [[CBForgetPWController alloc]init];
    [self presentViewController:forget animated:YES completion:nil];
    
}



- (IBAction)itemBtn:(UIButton *)sender {
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}



@end
