//
//  CBBingNameController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/10.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBBingNameController.h"
#import "ZFDropDown.h"

@interface CBBingNameController ()<ZFDropDownDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollviewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pictureHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *itemHeight;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *realName;
@property (weak, nonatomic) IBOutlet UILabel *cartificateTypeLable;
@property (weak, nonatomic) IBOutlet UIButton *cartifcateTypeBtn;
@property (weak, nonatomic) IBOutlet UIView *cartifcateBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *realNameLable;
@property (weak, nonatomic) IBOutlet UITextField *realNameTextfield;
@property (weak, nonatomic) IBOutlet UIView *realNameBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *numberLable;
@property (weak, nonatomic) IBOutlet UITextField *numberTextfield;
@property (weak, nonatomic) IBOutlet UIView *numberBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *itemLable;
@property (weak, nonatomic) IBOutlet UIView *segmentationView;
@property (weak, nonatomic) IBOutlet UILabel *pictureLable;
@property (weak, nonatomic) IBOutlet UIButton *positiveBtn;
@property (weak, nonatomic) IBOutlet UIButton *NegativeBtn;
@property (weak, nonatomic) IBOutlet UIButton *handPositiveBtn;
@property (weak, nonatomic) IBOutlet UILabel *handLable;
@property (weak, nonatomic) IBOutlet UILabel *longItemLable;
@property (weak, nonatomic) IBOutlet UIButton *determineBtn;

@property (nonatomic, strong) ZFDropDown * dropDown;
@property (nonatomic, strong) ZFTapGestureRecognizer * tap;
@end

@implementation CBBingNameController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UITapGestureRecognizer * tapGesturRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
//    [self.backgroundView addGestureRecognizer:tapGesturRecognizer];
    [self greatUI];
    
}


- (void)greatUI{
    
    self.realName.text = @"实名认证";
    self.realName.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.cartificateTypeLable.text = @"证件类型";
    self.cartificateTypeLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.realNameLable.text = @"真实姓名";
    self.realNameLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.cartifcateBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.realNameBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.numberBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.numberLable.text = @"证件号码";
    self.numberLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.segmentationView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.itemLable.text= @"注：请务必使用您本人的实名账户";
    self.itemLable.textColor = [UIColor colorWithHexString:@"#FFA55A"];
    self.pictureLable.text = @"上传身份证照片";
    self.pictureLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.handLable.text = @"上传手持身份证照片";
    self.handLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.longItemLable.text = @"注：上传图片内证件号，姓名等信息应确保清晰；不可修改或覆盖";
    self.longItemLable.textColor = [UIColor colorWithHexString:@"#9299A7"];
    
    [self.determineBtn setTitle:@"身份证" forState:UIControlStateNormal];
    [self.determineBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.determineBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    
    self.positiveBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.positiveBtn.layer.shadowOffset = CGSizeMake(0,0);
    self.positiveBtn.layer.shadowOpacity = 0.5;
    self.positiveBtn.layer.shadowRadius = 3;
    
    self.NegativeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.NegativeBtn.layer.shadowOffset = CGSizeMake(0,0);
    self.NegativeBtn.layer.shadowOpacity = 0.5;
    self.NegativeBtn.layer.shadowRadius = 3;
    self.handPositiveBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    self.handPositiveBtn.layer.shadowOffset = CGSizeMake(0,0);
    self.handPositiveBtn.layer.shadowOpacity = 0.5;
    self.handPositiveBtn.layer.shadowRadius = 3;
    
    [self chooseType];
}

//选择证件类型
- (void)chooseType{
    self.dropDown = [[ZFDropDown alloc] initWithFrame:CGRectMake(13, 120, self.cartifcateTypeBtn.frame.size.width - 40, self.cartifcateTypeBtn.frame.size.height) pattern:kDropDownPatternDefault];
    self.dropDown.delegate = self;
    [self.dropDown.topicButton setTitle:@"身份证" forState:UIControlStateNormal];
    [self.dropDown.topicButton setTitleColor:[UIColor colorWithHexString:@"#6D778B"] forState:UIControlStateNormal];
    self.dropDown.topicButton.titleLabel.font = [UIFont systemFontOfSize:16.f];
    self.dropDown.cornerRadius = 5.f;
    [self.backgroundView addSubview:self.dropDown];
    
    self.tap = [[ZFTapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [self.backgroundView addGestureRecognizer:self.tap];
    
}

//选择证件类型
- (IBAction)cartifcateTypeBTN:(UIButton *)sender {
    
    
}

- (void)tapAction{
    [self.dropDown resignDropDownResponder];
}


#pragma mark - ZFDropDownDelegate
- (NSArray *)itemArrayInDropDown:(ZFDropDown *)dropDown{
    return @[@"身份证", @"护照", @"港澳通行证"];
}

- (NSUInteger)numberOfRowsToDisplayIndropDown:(ZFDropDown *)dropDown itemArrayCount:(NSUInteger)count{
    return 3;
}




- (IBAction)backBTN:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//正面
- (IBAction)positiveBTN:(UIButton *)sender {
    
    
}

//反面
- (IBAction)negativeBTN:(UIButton *)sender {
    
    
}

//手持
- (IBAction)handPositiveBTN:(UIButton *)sender {
    
}



- (IBAction)determineBTN:(UIButton *)sender {
    
    
}

//-(void)tapAction:(id)tap{
//    
//    [self.backgroundView endEditing:YES];
//}


@end
