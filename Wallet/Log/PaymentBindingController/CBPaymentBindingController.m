//
//  CBPaymentBindingController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBPaymentBindingController.h"
#import "CBPayBingController.h"
#import "CBBingNameController.h"
//#import "CBContactController.h"
#import "CBMainTabController.h"


@interface CBPaymentBindingController ()
@property (weak, nonatomic) IBOutlet UILabel *paymentBingLable;
@property (weak, nonatomic) IBOutlet UIButton *payBingBtn;
@property (weak, nonatomic) IBOutlet UIButton *nameBingBtn;

@property (weak, nonatomic) IBOutlet UIImageView *payImage;
@property (weak, nonatomic) IBOutlet UIImageView *nameImage;

@property (weak, nonatomic) IBOutlet UIButton *beginUse;

@end

@implementation CBPaymentBindingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self greatUI];
}

- (void)greatUI{
    
    self.paymentBingLable.text = @"请完成\n实名认证及支付方式绑定";
    self.paymentBingLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    
    [self.beginUse setTitle:@"开始使用" forState:UIControlStateNormal];
    [self.beginUse setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.beginUse.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    self.beginUse.layer.borderColor = [[UIColor clearColor] CGColor];
    self.beginUse.layer.borderWidth = 1.0f;
    self.beginUse.layer.cornerRadius = 2.0f;
    
}



- (IBAction)payBingBTN:(UIButton *)sender {
    
    CBPayBingController * pay = [[CBPayBingController alloc]init];
    [self presentViewController:pay animated:YES completion:nil];
    
}


- (IBAction)nameBingBTN:(UIButton *)sender {
    
    CBBingNameController * name = [[CBBingNameController alloc]init];
    [self presentViewController:name animated:YES completion:nil];

}


- (IBAction)backBtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)toUseBTN:(UIButton *)sender {
    
    CBMainTabController * tab = [[CBMainTabController alloc]init];
    [self presentViewController:tab animated:YES completion:nil];
}


@end
