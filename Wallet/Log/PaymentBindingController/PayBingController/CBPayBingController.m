//
//  CBPayBingController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBPayBingController.h"
#import "CBBankBingViewController.h"
#import "CBAllPayBingController.h"

@interface CBPayBingController ()

@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
@property (weak, nonatomic) IBOutlet UIButton *zhifubaoBtn;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UILabel *banklLable;
@property (weak, nonatomic) IBOutlet UILabel *zhifubaoLable;
@property (weak, nonatomic) IBOutlet UILabel *wechatLable;
@property (weak, nonatomic) IBOutlet UILabel *bankType;
@property (weak, nonatomic) IBOutlet UILabel *zhifubaoType;
@property (weak, nonatomic) IBOutlet UILabel *wechatType;
@property (weak, nonatomic) IBOutlet UILabel *itemLable;

@end

@implementation CBPayBingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self greatUI];
}

- (void)greatUI{
    
    self.bankBtn.backgroundColor = [UIColor colorWithHexString:@"#F5F6FA"];
    self.zhifubaoBtn.backgroundColor = [UIColor colorWithHexString:@"#F5F6FA"];
    self.wechatBtn.backgroundColor = [UIColor colorWithHexString:@"#F5F6FA"];
    self.banklLable.text = @"银行卡";
    self.zhifubaoLable.text = @"支付宝";
    self.wechatLable.text = @"微信";
    self.banklLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
    self.zhifubaoLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
    self.wechatLable.textColor = [UIColor colorWithHexString:@"#9FA9BA"];
    
    self.bankType.text = @"立即绑定";
    self.zhifubaoType.text = @"立即绑定";
    self.wechatType.text = @"立即绑定";
    
    self.bankType.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.zhifubaoType.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.wechatType.textColor = [UIColor colorWithHexString:@"#6D778B"];
    
    self.itemLable.text = @"注：请务必使用您本人的实名账户，被绑定的支付方式将在交易时向买方展示，请至少绑定一种支付方式";
    self.itemLable.textColor = [UIColor colorWithHexString:@"#9299A7"];
}



- (IBAction)backBTN:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}





- (IBAction)bankBTN:(UIButton *)sender {
    
    CBBankBingViewController * bank = [[CBBankBingViewController alloc]init];
    [self presentViewController:bank animated:YES completion:nil];
    
}


- (IBAction)zhifubaoBTN:(UIButton *)sender {
    
    CBAllPayBingController * bank = [[CBAllPayBingController alloc]init];
    [self presentViewController:bank animated:YES completion:nil];
}

- (IBAction)wechatBTN:(UIButton *)sender {
    
    
}



@end
