//
//  CBBankBingViewController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBBankBingViewController.h"

@interface CBBankBingViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollviewHeight;
@property (weak, nonatomic) IBOutlet UIView *BottonView;


@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UITextField *nameTextfield;
@property (weak, nonatomic) IBOutlet UIView *nameBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *bankNumberLable;
@property (weak, nonatomic) IBOutlet UITextField *bankTextfield;
@property (weak, nonatomic) IBOutlet UIView *bankBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *openBankLable;
@property (weak, nonatomic) IBOutlet UITextField *openTextfield;
@property (weak, nonatomic) IBOutlet UIView *openBankBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *branceBankLable;
@property (weak, nonatomic) IBOutlet UITextField *branceBankTextfield;
@property (weak, nonatomic) IBOutlet UIView *branceBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *itemLable;
@property (weak, nonatomic) IBOutlet UIButton *determineBtn;

@end

@implementation CBBankBingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * tapGesturRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    [self.BottonView addGestureRecognizer:tapGesturRecognizer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bankTextFieldTextChange) name:UITextFieldTextDidChangeNotification object:nil];
    
    [self greatUI];
}

-(void)tapAction:(id)tap{
    
    [self.BottonView endEditing:YES];
}

- (void)registTextFieldTextChange{
    
    if (self.nameTextfield.text.length > 0 && self.bankTextfield.text.length > 0 && self.openTextfield.text.length > 0 && self.branceBankTextfield.text.length > 0) {
        self.determineBtn.enabled = YES;
        [self.determineBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
        self.determineBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    }else{
        self.determineBtn.enabled = NO;
        [self.determineBtn setTitleColor:[UIColor colorWithHexString:@"#DBDBDB"] forState:UIControlStateNormal];
        self.determineBtn.backgroundColor = [UIColor colorWithHexString:@"#DBDBDB"];
    }
    
    
}

- (void)greatUI{
    
    self.scrollviewHeight.constant = 800;
    self.titleLable.text = @"请输入您的信息";
    self.titleLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.nameLable.text = @"姓名";
    self.nameLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.nameBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.bankNumberLable.text = @"银行卡号";
    self.bankNumberLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.bankBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.openBankLable.text = @"开户银行";
    self.openBankLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.openBankBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.branceBankLable.text = @"开户支行";
    self.branceBankLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.branceBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.itemLable.text = @"注：请务必使用您本人的实名账号";
    self.itemLable.textColor = [UIColor colorWithHexString:@"#FFA55A"];
    self.determineBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    [self.determineBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.determineBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    
    self.nameTextfield.delegate = self;
    self.bankTextfield.delegate = self;
    self.openTextfield.delegate = self;
    self.branceBankTextfield.delegate = self;
}

- (IBAction)determineBTN:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)backBTN:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


//点击return 按钮 去掉
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
