//
//  CBAllPayBingController.m
//  Wallet
//
//  Created by 壹九科技1 on 2019/4/9.
//  Copyright © 2019 壹九科技1. All rights reserved.
//

#import "CBAllPayBingController.h"

@interface CBAllPayBingController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollviewHeihgt;
@property (weak, nonatomic) IBOutlet UIView *bigButtonView;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *nameTextfield;
@property (weak, nonatomic) IBOutlet UIView *nameBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *payLable;
@property (weak, nonatomic) IBOutlet UITextField *payTextfield;
@property (weak, nonatomic) IBOutlet UIView *payBottonLine;
@property (weak, nonatomic) IBOutlet UILabel *itemLable;

@property (weak, nonatomic) IBOutlet UIView *bottonViewLine;
@property (weak, nonatomic) IBOutlet UILabel *QRCodeLable;

@property (weak, nonatomic) IBOutlet UIButton *clickBtn;
@property (weak, nonatomic) IBOutlet UIButton *determineBtn;


@end

@implementation CBAllPayBingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer * tapGesturRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
    [self.bigButtonView addGestureRecognizer:tapGesturRecognizer];
    
    [self greatUI];
}

-(void)tapAction:(id)tap{
    
    [self.bigButtonView endEditing:YES];
}


- (void)greatUI{
    self.scrollviewHeihgt.constant = 700;
    self.name.text = @"姓名";
    self.name.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.nameBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.payLable.text = @"支付宝账号";
    self.payLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.payBottonLine.backgroundColor = [UIColor colorWithHexString:@"#E3E3E3"];
    self.itemLable.textColor = [UIColor colorWithHexString:@"#FFA55A"];
    self.itemLable.text = @"注：请务必使用您本人的实名账号";
    self.bottonViewLine.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    
    self.QRCodeLable.textColor = [UIColor colorWithHexString:@"#6D778B"];
    self.QRCodeLable.text = @"请上传收款二维码";
    
    [self.determineBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.determineBtn setTitleColor:[UIColor colorWithHexString:@"#FFFFFF"] forState:UIControlStateNormal];
    self.determineBtn.backgroundColor = [UIColor colorWithHexString:@"#3B67E0"];
    
    
}


- (IBAction)backBTN:(UIButton *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}





- (IBAction)clickBTN:(UIButton *)sender {
    
    
    
}

- (IBAction)determineBTN:(UIButton *)sender {
    
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
    
}

@end
